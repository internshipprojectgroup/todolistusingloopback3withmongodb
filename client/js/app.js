var myTodo = angular.module("myTodo",['lbServices']);
myTodo.controller("mainController",function($scope,$http,Todo){
  $scope.todos = Todo.find();
  $scope.newTodo ='' ;
  $scope.pushTodos =function() {
    if($scope.newTodo !=" "){
       Todo.create({text:$scope.newTodo}).$promise.then(function(name){
         $scope.todos.push(name);
         $scope.newTodo = ' ';
       })
    }
  }
  $scope.deleteTodo = function(index) {
    Todo.deleteById({id:$scope.todos[index].id}).$promise.then(function(){
      $scope.todos.splice(index,1);
    })
  }

});
